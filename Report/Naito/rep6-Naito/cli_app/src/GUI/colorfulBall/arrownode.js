createsvg();

function createsvg () {
  var svg = d3.select("#example").append("svg")
      .attr({
        width: 1000,
        height: 1000
      });

//カラフル玉を10こ作成するプログラム
  var carray = creatRondCircle(10);

  // 10種類の色を返す関数を使う
  var color = d3.scale.category10();
  var circle = svg.selectAll('circle').data(carray).enter().append('circle')
    .attr({
      'cx': function(d) { return d[0]; },
      'cy': function(d) { return d[1]; },
      'r': function(d) { return d[2]; },
      // dはデータ要素そのもの、iはindex番号を返す
      // color(i)で、n番目の色データを返す
      'fill': function(d,i) { return color(i); },
    });
}

function creatRondCircle (circleNum){
  var array = [];
  for (var i = 0; i < circleNum; i++) {
    //　0から10の値を生成する
    var rand1 = Math.floor( Math.random() * 800 ) ;
    var rand2 = Math.floor( Math.random() * 800 ) ;
    var rand3 = Math.floor( Math.random() * 300 ) ;
    var c = [rand1, rand2, rand3];
    array.push(c);
  }
  return array;
}
