function createNetwork(){

  var nodes;
  var edges;

//read local file
  $.ajax({
        url : "./db/groupNet/Node/nodes.json",
        dataType: 'json',
        cache:false,
        async:false
      }).done(function(data){
        nodes = data;
      });

  $.ajax({
        url : "./db/groupNet/Edge/edges.json",
        dataType: 'json',
        cache:false,
        async:false
      }).done(function(data){
        edges = data;
      });

  // create a network
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: nodes,
    edges: edges
  };
// optionのgroupsのgroupはlabelと同名にして、色は自動生成にする
  var options = {};
  var network = new vis.Network(container, data, options);
  //return network;
}
