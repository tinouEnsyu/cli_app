function createNetwork(){

  var nodes1;
  var nodes2;
  var edges;
  var directionInput = document.getElementById("direction");
  //read local file
//ワーキングメモリの中のアサーションの集合をとってくる-->
  $.ajax({
        url : "./db/workingMemory/wmTest2.json",
        dataType: 'json',
        cache:false,
        async:false
      }).done(function(data){
        nodes1 = data;
      });

//ルールベースの中のルールの集合をとってくる-->
  $.ajax({
        url : "./db/ruleBase/rbTest2.json",
        dataType: 'json',
        cache:false,
        async:false
      }).done(function(data){
        nodes2 = data;
      });


//なぜか同一ディクレトリへのajaxでのアクセスを拒否される
//エッジの情報をとってくる-->
  $.ajax({
        url : "./db/Edge/test2.json",
        dataType: 'json',
        cache:false,
        async:false
      }).done(function(data){
        edges = data;
      });

  // create a network-->
  var nodes = nodes1.concat(nodes2);//アサーションとルールの集合を結合 -->
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: nodes,
    edges: edges,
  };

//ネットワーク図の形のオプション -->
  var options = {
    nodes:{
      shape: 'box',
      shadow: true
    },
    edges: {
        smooth: {
            type: 'dynamic'
        },
        shadow:true,
        width:2
    },
    layout: {
        hierarchical: {
            direction:"DU"//DownUpの略-->
        }
    },
    physics: {
        hierarchicalRepulsion: {
            nodeDistance:180//ノード間の距離-->
        }
    }
  };
  var network = new vis.Network(container, data, options);
  console.log(nodes);
  console.log(edges);
  <!--return network;-->
}
