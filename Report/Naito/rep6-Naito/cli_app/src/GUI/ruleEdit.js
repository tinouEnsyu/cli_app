
document.addEventListener('DOMContentLoaded',addEvents,false);//ドキュメントロード時のイベントを追加

function addEvents(){
    makeRuleFileList();
    document.getElementById('load').addEventListener('click',makeRuleList,false);//クリック時のイベントを追加
    document.getElementById('apply').addEventListener('click',applyRuleChange,false);
    document.getElementById('add').addEventListener('click',addNewRule,false);
    document.getElementById('delete').addEventListener('click',deleteRule,false);
    document.getElementById('btn_write').addEventListener('click',writeJSON,false);
    //document.getElementById('file').addEventListener('change',readJSON2,false);
    resetValue();
}

var update = {read:false,edit:false};//更新記録(読み込み、変更)
var file_list = new Array();//ファイル名のリスト
var rule_list = new Array();//ルールのリスト

//ルールファイルのリストを作る
function makeRuleFileList(){
    file_list = readJSON('nameList.json');//JSONから得た配列を入手
    var list_obj = document.getElementById('rule_files');//ルールファイル一覧を入れるオブジェクトを入手
    for(var i = 0,len = file_list.length; i < len; ++i){
        var obj = document.createElement('option');
        //console.log('file_list'+i+' ='+file_list[i].label);
        obj.textContent = file_list[i].label;//ファイル名を代入する
        list_obj.appendChild(obj);
    }
}

//option属性の配列optionsの中から選ばれたものを返す
function getSelectedOption(options){
    for(var i = 0,len = options.length; i < len ; ++i){
        var opt = options.item(i);
        if(opt.selected){//選ばれたものであった場合
            return(opt.value);//内容を返す
        }
    }
    return(null);//何も選ばれていなければ null を返す
}

function makeRuleList(){
    document.getElementById('add').disabled = false;//ルールの新規追加を可能にする
    var rule_selector = {element:'input', type:"submit",class:"rule"};
    var model_ary = new Array(       'rule','submit');
    var att_ary = new Array('value','class','type');
    var event_ary = new Array('click');
    var func_ary = new Array(button_event2);
    var options = document.getElementById('rule_files').getElementsByTagName('option');
    var select_file = getSelectedOption(options);//メニューの中から選択されたファイル名を得る
    //console.log('file='+select_file);
    rule_list = readJSON2(select_file);//JSONから得た配列を入手
    var box = document.getElementById('rules');//ルール一覧ボックスを入手
    delete_children(box);//子ノードを全て削除する
    //console.log(minis.length);
    create_RuleButtons(box,rule_selector,button_event2);//子ノードを新たに作る
}

//ルールの変更を適用する
function applyRuleChange(){
    var ruleId = document.getElementById('rule_id');//ルールid のオブジェクトを入手する
    var ruleName = document.getElementById('ruleName');//ルール名のオブジェクトを入手する
    var if_box = document.getElementById('if');//if 文ボックスを入手
    var then_box = document.getElementById('then');//then 文ボックスを入手
    var rule_btn = document.getElementById(ruleName.getAttribute('paretn_id'));//ルールボタンを id から求める
    var apply_num = get_rule_num(ruleId.getAttribute('value'));//id から適用を行うルールの添字を求める
    
    rule_list[apply_num].name = ruleName.value; //ルール名を更新する
    
    var if_list = if_box.childNodes;//if文ボックス中の if文リストを入手
    var new_if_ary = new Array();//新しいif文配列
    for(var i = 0,len = if_list.length;i < len;++i){
        new_if_ary.push(if_list[i].value);//if文を全て配列に入れる
    }
     rule_list[apply_num].lhs = new_if_ary; //if文を更新する
    
    var then_list = then_box.childNodes;//then文ボックス中の then文リストを入手
    var new_then_ary = new Array();//新しいthen文配列
    for(var i = 0,len = then_list.length;i < len;++i){
        new_then_ary.push(then_list[i].value);//then文を全て配列に入れる
    }
     rule_list[apply_num].rhs = new_then_ary; //then文を更新する
    
    //rule_list の変更をルール一覧ボックスに適用する
    redrawRuleBox();
}

//値のリセットを行う
function resetValue(){
    //ルールidをリセットする
    var ruleId = document.getElementById('rule_id');
    ruleId.setAttribute('value',null);
    
    //ルール名をリセットする
    var ruleName = document.getElementById('ruleName');
    ruleName.value = null;
    ruleName.setAttribute('parent_id',null);
    
    //if 文ボックスをリセットする
    var if_box = document.getElementById('if');//if 文ボックスを入手
    delete_children(if_box);//子ノードを全て削除する
    
    //then 文ボックスをリセットする
    var then_box = document.getElementById('then');//then 文ボックスを入手
    delete_children(then_box);//子ノードを全て削除する
}

function resetUpdate(){
    update.read = false;
    update.edit = false;
}

//新しいルールを末尾に追加する
function addNewRule(){
    var max = get_Biggest_Id();
    //console.log("Biggest="+max);
    var new_rule_data = {id:max+1, name:"NewRule",lhs:[''],rhs:['']};//ルールデータ{ルール名,前件部,後件部}
    rule_list.push(new_rule_data);
    
    //rule_list の変更をルール一覧ボックスに適用する
   redrawRuleBox();
    
    var newButton = document.getElementById('rule_btn-'+(max+1).toString());//新たに追加されたルールのボタンを入手する
    newButton.click();//クリックイベントを起こす
}

//現在選択中のルールを削除する
function deleteRule(){
    var select_Id_obj = document.getElementById('rule_id');//現在選択中のルールのオブジェクトを入手
    var select_id = select_Id_obj.getAttribute('value');//オブジェクトの rule_list 中の id を入手
    var del_num = get_rule_num(select_id);
    //console.log("delete="+del_num);
    if(del_num != undefined){
        rule_list.splice(del_num,1);

       //rule_list の変更をルール一覧ボックスに適用する
       redrawRuleBox();
       resetValue();   
    }
}

function redrawRuleBox(){
    var rule_selector = {element:'input', type:"submit",class:"rule"};
    var box = document.getElementById('rules');//ルール一覧ボックスを入手
    delete_children(box);//子ノードを全て削除する
    create_RuleButtons(box,rule_selector,button_event2);//子ノードを新たに作る
}

//parent の子ノード children を全て削除する
function delete_children(parent){
    while(parent.firstChild){//子供がいる限り続ける
        var child = parent.firstChild;
        //child.removeAttribute('value');
        parent.removeChild(child);//子供を削除する
    }
}

//ルールボタン制作  parent の下に modelの情報と rule_list を元にした子ノードを追加する
function create_RuleButtons(parent,model,func_name){
    for(var i = 0,len = rule_list.length; i < len; ++i){
        //console.log("i="+i+" len="+len);
        var obj = document.createElement(model.element);
        obj.setAttribute('value',rule_list[i].name);
        obj.setAttribute('class', 'rule');
        obj.setAttribute('type',model.type);
        obj.setAttribute('data-id',rule_list[i].id);
        obj.setAttribute('id','rule_btn-'+rule_list[i].id.toString());
        //obj.setAttribute('onclick',"button_event("+i+")");
        if(func_name != null){
            obj.addEventListener('click',func_name,false);
        }
        parent.appendChild(obj);
    }
}

//if,then のリストを作る parent の下に modelの情報と data を元にした子ノードを追加する
function create_TextList(parent,model,data,func_name){
    for(var i = 0,len = data.length; i < len; ++i){
        //console.log("i="+i+" len="+len);
        var obj = document.createElement(model.element);
        obj.setAttribute('value',data[i]);
        obj.setAttribute('class', 'rule');
        obj.setAttribute('type',model.type);
        //obj.setAttribute('onclick',"button_event("+i+")");
        if(func_name != null){
            obj.addEventListener('keydown',func_name,false);
        }
        parent.appendChild(obj);
    }
}

function keydown_event(e){
    //var text =  e.target.value;//現在の文字フォーム内の文字を入手する
    //console.log('you edited '+text);
    var key = e.keyCode;//押されたキーのコードを入手する
    //console.log('you pushed '+key);
    switch(key){
        case 13:{//13(Enter) が押された場合
                var if_selector = {element:'input', type:"text",class:"rule"}; 
                var obj = document.createElement(if_selector.element);//新しい要素を作る
                obj.setAttribute('class', if_selector.class);//属性を設定する
                obj.setAttribute('type',if_selector.type);
                obj.addEventListener('keydown',keydown_event,false);//イベントを追加する
                var next_Node = e.target.nextSibling;//使用中のノードの次のノードを入手する
                e.target.parentNode.insertBefore(obj,next_Node);//ノードを追加する
                obj.focus();//カーソルを移動
                break;
        }
        case 8:{//8(BackSpace) が押された場合
                var tgt = e.target;
                var text_len = tgt.value.length;
                var ownsNum = tgt.parentNode.childNodes.length;//自分を含めた仲間のテキストボックスの数を求める
                if(text_len == 0 && ownsNum > 1){//文章がゼロの状態で更に消そうとしていて、このテキストボックスを消してもまたテキストボックスが残る場合
                    var prev = tgt.previousSibling;
                    var next = tgt.nextSibling;
                   tgt.parentNode.removeChild(tgt);//ノードを削除する
                    if(next != null){
                        next.focus();
                    }
                    else if(prev != null){
                        prev.focus();
                    }
                }
                break;
        }
        case 38:{//38(上矢印) が押された場合
                var prev_Node = e.target.previousSibling;//使用中のノードの前のノードを入手する
                if(prev_Node != null){//前のノードが存在した場合
                    prev_Node.focus();//カーソルを移動
                }
                break;
        }
        case 40:{//40(下矢印) が押された場合
                var next_Node = e.target.nextSibling;//使用中のノードの次のノードを入手する
                if(next_Node != null){//次のノードが存在した場合
                    next_Node.focus();//カーソルを移動
                }
                break;
        }
    }
  
    
}

//rule_list 中で最大のIDを返す(ID は全て数字である前提)
function get_Biggest_Id(){
    var max = -1;
    for(var i = 0,len = rule_list.length;i < len ; ++i){
        var id = parseInt(rule_list[i].id);
        if(max < id){
            max = id;
        }
    }
    return(max);
}

//id に一致するルール名のルールの添字を得る
function get_rule_num(id){
    var num;
    for(var i =0,len = rule_list.length; i < len; ++i){
        var list_id = rule_list[i].id;
        if(list_id == id){//名前が一致した場合
            num = i;
        }
    }
    return(num);
}

//id に一致するルールidのルールの if 文部を入手する
function get_if(id){
    var if_list;
    for(var i =0,len = rule_list.length; i < len; ++i){
        var list_id = rule_list[i].id;
        if(list_id == id){//名前が一致した場合
            if_list = rule_list[i].lhs;//前件(if 文)を入手
        }
    }
    return(if_list);
}

//name に一致するルール名のルールの then 文部を入手する
function get_then(id){
    var then_list;
    for(var i =0,len = rule_list.length; i < len; ++i){
        var list_id = rule_list[i].id;
        if(list_id == id){//名前が一致した場合
            then_list = rule_list[i].rhs;//後件(then 文)を入手
        }
    }
    return(then_list);
}

//ボタンを押された時に呼び出す関数 引数eはイベントを表し、自動的に設定される
function button_event2(e){
    //ルール id を設定する
   var dataid = e.target.getAttribute('data-id');//クリックされたボタンの data-iid 要素を得る
   var id = document.getElementById('rule_id');
   id.setAttribute('value',dataid);
    
    //ルール名を設定する
   var name = document.getElementById('ruleName');
   var ruleName = e.target.getAttribute('value');
   name.value = ruleName;//ルール名(表示用)を更新
   name.setAttribute('value',ruleName);//ルール名(デフォルト値)を更新
   name.setAttribute('parent_id',e.target.getAttribute('id'));

    //if 文を設定する
    var if_selector = {element:'input', type:"text",class:"rule"}; 
    var if_box = document.getElementById('if');//if 文ボックスを入手
    delete_children(if_box);//子ノードを全て削除する
    var if_list = get_if(dataid);
    //console.log("if_len="+if_list.length);
    create_TextList(if_box,if_selector,if_list,keydown_event);//if文のリストを作る
    
    //then 文を設定する
    var then_selector = {element:'input', type:"text",class:"rule"}; 
    var then_box = document.getElementById('then');//then 文ボックスを入手
    delete_children(then_box);//子ノードを全て削除する
    var then_list = get_then(dataid);
    //console.log("then_len="+then_list.length);
    create_TextList(then_box,then_selector,then_list,keydown_event);//then文のリストを作る
}

function writeJSON(){
     $.ajax({
            type: "PUT",
            dataType: 'json',//ファイルのタイプ JSON形式で読み込む
            url: "./DB/save.json",
            data: rule_list,
            cache:false,
            async:false
           /*
            ,
            success: function(msg){
                makeOption();
                $('ul#result input').val('');
                $('div#msg').html(msg).css('display','block');
                setTimeout("$('div#msg').fadeOut(1000)", 2000);
            }
            //*/
        });
}

function readJSON(file_name){
     var nodes;
    //ローカルファイルを読み込む(ブラウザ依存あり Firefoxでしかまともに動かない)
    $.ajax({
        url : './DB/'+file_name,//所在地(相対パス)
        dataType: 'json',//ファイルのタイプ JSON形式で読み込む
        cache:false,
        async:false
    }).done(function(data){
        nodes = data;//nodes に入れるこの時既に JSON 型
    });
    return(nodes);
}

//ローカルの JSON　ファイルを読み出しオブジェクトにして返す
function readJSON2(file_name){
    var nodes;
    //ローカルファイルを読み込む(ブラウザ依存あり Firefoxでしかまともに動かない)
    $.ajax({
	//url : './DB/data1.json',//所在地(相対パス)
        url : './DB/'+file_name,//所在地(相対パス)
        dataType: 'json',//ファイルのタイプ JSON形式で読み込む
        cache:false,
        async:false
    }).done(function(data){
        nodes = data;//nodes に入れるこの時既に JSON 型
    });

    //console.log(nodes);
    var rule_ary = new Array();//ルールのオブジェクトの行列
    for(var i =0,len = nodes.length;i < len; ++i){
        rule_ary.push(convertLabels(nodes[i].id,nodes[i].label));
    }
    //return network;
    //*/
    return(rule_ary);
}

//読み込んだJSONデータをオブジェクトにする
function convertLabels(arg_id,arg_label){
    var rule;
    var if_ary = new Array();
    var then_ary = new Array();
    
    var pat_nl = /\n/gm;//改行コードの正規表現(文字列全体に対してマッチ、大文字小文字を区別する、複数行に対応)
    var pat_rule = /rule(.|\n)+if:/;//ruleからifまで:(任意の(と改行コード)１文字以上の文字列) の正規表現
    var pat_if = /if:(.|\n)+then:/g;//rule部(任意の(改行コード)１文字以上の文字列) の正規表現
    var pat_then = /then:(.|\n)+$/g;//then部(任意の(改行コード)１文字以上の文字列) の正規表現
    var pat_rule_del = /rule:/;
    var pat_if_del = /if/;
    var pat_del = /(rule:|if:|then:|^(\s)+)+/gm;//消去対象(rule:, if:, then:,if文末, 行頭の空白)(複数回マッチする 大文字小文字を区別する)
    var pat_str = /.+/gm;
    //*
    var res1 = arg_label.match(pat_rule);//rule~if を入手
    var rule = res1[0].replace(pat_del,"");//余分なものを消す
    //console.log("Rule="+rule);
    //*/
    var res2 = arg_label.match(pat_if);//if~then を入手
    //console.log("len2="+res2.length);
    //console.log("res2="+res2[0]);
    var res2_2 = res2[0].match(pat_str);//行ごとに分割する
    //console.log("len2_3="+res2_3.length);
    for(var i = 0,len = res2_2.length;i < len;++i){
        //console.log(res2_2[i]);
        var res2_2i = res2_2[i].replace(pat_del,"");
        //console.log("deled="+res2_2i);
        if(res2_2i.length > 0){
            if_ary.push(res2_2i);
        }
    }
    
   var res3 = arg_label.match(pat_then);//then~文末を入手
   //console.log("len3="+res3.length);
   //console.log("res3="+res3[0]);
    var res3_2 = res3[0].match(pat_str);//行ごとに分割する
    //console.log("len3_1="+res3_2.length);
    for(var i = 0,len = res3_2.length;i < len;++i){
        //console.log(res3_2[i]);
        var res3_2i = res3_2[i].replace(pat_del,"");
        //console.log("deled="+res3_2i);
        if(res3_2i.length > 0){
            then_ary.push(res3_2i);
        }
    }
    
    /*
    console.log("Rule="+rule);
    console.log("if="+if_ary.toString());
    console.log("then="+then_ary.toString());
    //*/
    var rule_data = {id:arg_id, name:rule,lhs:if_ary,rhs:then_ary};//ルールデータ{ルール名,前件部,後件部}
    return(rule_data);
}

