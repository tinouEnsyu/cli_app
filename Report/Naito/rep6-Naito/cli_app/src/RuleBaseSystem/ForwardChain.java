package RuleBaseSystem;

import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.StringTokenizer;

/**
 * Created by seijihagawa on 2016/11/24.
 */
public class ForwardChain {

    private WorkingMemory mWorkingMemory;
    private RuleBase mRuleBase;


    /**
     * @param aWorkingMemory 推論に利用するワーキングメモリ
     * @param aRuleBase      推論に利用するルールベース
     */
    ForwardChain(WorkingMemory aWorkingMemory, RuleBase aRuleBase) {
        mWorkingMemory = aWorkingMemory;
        mRuleBase = aRuleBase;

        try {
            // ルールの読み込み
            mRuleBase.loadRules();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }


        goForwardChain();
    }


    /**
     * 前向き推論実行
     * boolean Forward()（＝tNewAssertionCreated）がtrueであればを繰り返すForward()を繰り返す
     */
    public void goForwardChain() {
        boolean tNewAssertionCreated = false;
        // ルールベースから前件を取り出す
        ArrayList<ArrayList<String>> tAntecedents = mRuleBase.getAntecedents();
//		System.out.println(tAntecedents);//********************************

        do {
            tNewAssertionCreated = Forward(tAntecedents);
            System.out.println("Working Memory" + mWorkingMemory.getAsseertion());
        } while (tNewAssertionCreated);
        // 新しいアサーションが生成されなくなったら終了
        System.out.println("No rule produces a new assertion");
    }


    /**
     * 前向き推論実行
     *
     * @param aAntecedents 前件のリストのリスト
     * @return tNewAssertionCreated 新しいアサーションが生成されたなら true そうでないならfalse
     */
    private boolean Forward(ArrayList<ArrayList<String>> aAntecedents) {
        boolean tNewAssertionCreated = false;
        int i = -1;
        for (ArrayList<String> tAntecedent : aAntecedents) {
            // 変数束縛情報を得る
            ArrayList tBindings = mWorkingMemory.searchBinding(tAntecedent);
            // 今見ているルールの後件を取り出す
            String tConsequent = mRuleBase.getConsequent(++i);

            // 変数束縛情報を得られたなら
            if (tBindings != null) {
                for (int j = 0; j < tBindings.size(); j++) {
                    // 後件を具体化
                    String tNewAssertion =
                            instantiate(tConsequent, (HashMap) tBindings.get(j));
                    // ワーキングメモリになければ成功
                    if (!mWorkingMemory.contains(tNewAssertion)) {
                        System.out.println("Success: " + tNewAssertion);
                        mWorkingMemory.addAssertion(tNewAssertion);
                        System.out.println("ADD:" + tNewAssertion);
                        tNewAssertionCreated = true;
                    }
                }
            }
        }
        return tNewAssertionCreated;
    }


    /**
     * 変数束縛情報のリストから与えられた後件を具体化する
     *
     * @param aThePattern  後件
     * @param aTheBindings 変数束縛情報のリスト
     * @return 具体化された後件  tResult.trim()
     */
    private String instantiate(String aThePattern, HashMap aTheBindings) {
        String tResult = new String();
        StringTokenizer tStringTokenizer = new StringTokenizer(aThePattern);
        for (int i = 0; i < tStringTokenizer.countTokens(); ) {
            String tTmp = tStringTokenizer.nextToken();
            if ((new Matcher()).var(tTmp)) {
                tResult = tResult + " " + (String) aTheBindings.get(tTmp);
            } else {
                tResult = tResult + " " + tTmp;
            }
        }
        return tResult.trim();
    }


}