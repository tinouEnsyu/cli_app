package RuleBaseSystem;

import java.util.StringTokenizer;

/**
 * Created by seijihagawa on 2016/11/29.
 */

/**
 * 2つのstringが一致するかどうかをcheckする
 */
public class Matcher {

    Matcher() {
    }


    /**
     * 変数束縛可能かどうかを返却する関数
     *
     * @param aVar
     * @param aSentence
     * @return
     */
    public boolean canBind(String aVar, String aSentence) {

        StringTokenizer tVarToken = new StringTokenizer(aVar);
        StringTokenizer tSentenceToken = new StringTokenizer(aSentence);

        if (tVarToken.countTokens() != tSentenceToken.countTokens()) return false;

        int tNum = tVarToken.countTokens();
        for (int i = 0; i < tNum; i++) {

            String tV = tVarToken.nextToken();
            String tS = tSentenceToken.nextToken();

            if (tV.startsWith("?")) {
                continue;
            }

            if (!tV.equals(tS)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param aVar1 変数を含んだセンテンス
     * @param aVar2 変数を含んだセンテンス
     * @return 文の冒頭にのみ、?がくる、という前提としている
     * どちらも?で始まるものがあった時のために,ひとまず、canbindと合体はさせずに残しておく
     */
    public boolean varCanBind(String aVar1, String aVar2) {

        StringTokenizer tVarToken1 = new StringTokenizer(aVar1);
        StringTokenizer tVarToken2 = new StringTokenizer(aVar2);

        if (tVarToken1.countTokens() != tVarToken2.countTokens()) return false;

        int tNum = tVarToken1.countTokens(); //可変的な値(っぽい)ので、外だし
        for (int i = 0; i < tNum; i++) {

            String tV1 = tVarToken1.nextToken();
            String tV2 = tVarToken2.nextToken();

            if (tV1.startsWith("?") || tV2.startsWith("?")) {
                continue;
            }

            if (!tV1.equals(tV2)) {
                return false;
            }
        }

        return true;

    }


    /**
     * 変数のインスタンス化を行う関数
     * <p>
     * ちゃんと動くのかは確かめていない
     * ここに実装するのはおかしい気はする.
     * これは1文の中に、１つの変数しか対応できない.
     * bindできる前提で動カスっていうこの粒度の設定ははどうなんだろう
     */
    public String bind(String aVar, String aSentence) {

        StringTokenizer tVarToken = new StringTokenizer(aVar);
        StringTokenizer tSentenceToken = new StringTokenizer(aSentence);

        while (!tVarToken.nextToken().startsWith("?")) {
            tSentenceToken.nextToken();
        }

        return tSentenceToken.nextToken();
    }


}