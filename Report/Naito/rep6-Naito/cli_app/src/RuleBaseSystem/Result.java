package RuleBaseSystem;

import com.fasterxml.jackson.annotation.JsonSetter;
import java.util.ArrayList;

public class Result {
    private  ArrayList<String> mAssertions;
    private  String mRule;
    private  String mVector;
    private  ArrayList<String> mBinds;

    Result() {

    }

    Result(ArrayList<String> aAssertions, String aRule, String aVector, ArrayList<String> aBinds) {
        this.mAssertions = aAssertions;
        this.mRule = aRule;
        this.mVector = aVector;
        this.mBinds = aBinds;
    }


    @JsonSetter("Assertions")
    public void setAssertions(ArrayList<String> aAssertions) {
        this.mAssertions = aAssertions;
    }

    @JsonSetter("Rule")
    public void setRule(String aRule) {
        this.mRule = aRule;
    }

    @JsonSetter("Vector")
    public void setVector(String aVector) {
        this.mVector = aVector;
    }

    @JsonSetter("Binds")
    public void setBinds(ArrayList<String> aBinds) {
        this.mBinds = aBinds;
    }

    public ArrayList<String> getAssertions() {
        return mAssertions;
    }

    public String getRule() {
        return mRule;
    }

    public String getVector() {
        return mVector;
    }

    public ArrayList<String> getBinds() {
        return mBinds;
    }

}
