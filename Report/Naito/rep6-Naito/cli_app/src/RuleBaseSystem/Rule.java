package RuleBaseSystem;

/**
 * Created by seijihagawa on 2016/11/24.
 */

import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;


/**
 * ルールを表すクラス.
 *
 * @see "http://tutorials.jenkov.com/java-json/jackson-annotations.html#read-annotations"
 * jackson用のアノテーションについての解説tip
 *
 * staticを消したらバグが消えた。なぜだろう。
 * staticがあると、すべてのRuleが同一のkNameID, kAntecedents,kConsequentを参照していた。
 * staticについて後日勉強.気持ちはstaticなので、prefixはkのままにしておく
 */
public class Rule {
    private  String kNameID;
    private  ArrayList<String> kAntecedents;
    private  String kConsequent;


    Rule(){

    }

    Rule(String aID, ArrayList<String> aAntecedents, String aConsequent){
        this.kNameID = aID;
        this.kAntecedents = aAntecedents;
        this.kConsequent = aConsequent;
    }

    @JsonSetter("ID")
    public void setID(String aID){
        this.kNameID = aID;
    }

    @JsonSetter("Antecedents")
    public void setAntecedents(ArrayList<String> aAntecedents){
        this.kAntecedents = aAntecedents;
    }

    @JsonSetter("Consequent")
    public void setConsequent(String aConsequent){
        this.kConsequent = aConsequent;
    }

    public String getNameID(){
        return kNameID;
    }

    public ArrayList<String> getAntecedents(){
        return kAntecedents;
    }

    public String getConsequent(){
        return kConsequent;
    }

}