package RuleBaseSystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by seijihagawa on 2016/11/24.
 */

/**
 * DBとしてのワーキングメモリを扱うためのクラス.．
 */
public class WorkingMemory {


    private ArrayList<String> mAssertions;
    private ReaderTxt mReader;
    private WriterTxt mWriter;
    private String mLoadDirPath;
    private String mSaveCategoryDirPath;
    private Matcher mMatcher;


    /**
     * mReaderを1次変数にしなくていいのかが、気になる
     * readerオブジェクトを用いて、ワーキングメモリを初期化
     *
     * @param aLoadDir
     * @param aSaveCategorDir DataBase/assertion/Categoryのように指定してください.
     *                        ex.車のカテゴリーの話をしている時は、~/Carまでとしてください.
     *                        それ以下のファイルのインデックスは自動生成されます.
     */
    WorkingMemory(String aLoadDir, String aSaveCategorDir) throws IOException {

        mReader = new ReaderTxt();
        mWriter = new WriterTxt();
        mLoadDirPath = aLoadDir;
        mSaveCategoryDirPath = aSaveCategorDir;
        mMatcher = new Matcher();

        //コンストラクタでloadしないほうがいいかもしれない
        try {
            mAssertions = this.loadAssertions();
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * aNum番目のアサーションを取得する.
     *
     * @param
     * @return mapの範疇外の値が呼び出された場合、ここは例外で処理していいものなのだろうか？
     */

    public ArrayList<String> getAsseertions() {
        return mAssertions;
    }

    public String getAsertion(int aID) {
        return mAssertions.get(aID);
    }

    /**
     * アサーションをワーキングメモリに加える．
     *
     * @param tAssertion String
     */
    public void addAssertion(String tAssertion) {
        mAssertions.add(tAssertion);
    }

    /**
     * アサーションが含まれているかどうかを調べる．
     *
     * @param tAssertion String
     * @return 含まれていれば true，含まれていなければ false
     */
    public boolean contains(String tAssertion) {
        return mAssertions.contains(tAssertion);
    }


    /**
     * @return
     * @throws IOException ファイルの拡張子をこの中で意識するのがちょっと変だから、本来は外だししたいところ
     */
    private ArrayList<String> loadAssertions() throws IOException {
        ArrayList<String> tAssertions = new ArrayList<String>();

        try {
            File tDir = new File(mLoadDirPath);
            File[] tFiles = tDir.listFiles();
            for (int i = 0; i < tFiles.length; i++) {
                String tPath = mLoadDirPath + "/" + String.valueOf(i) + ".txt";
                mReader.setFilePath(tPath);
                tAssertions.add(mReader.readOneString());
            }

            return tAssertions;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    /**
     * ワーキングメモリに格納されているアサーションを全てファイルとして出力する.
     * <p>
     * <p>
     * ディレクトリが存在しない場合、新しくディレクトリを作成しなくてはいけない
     */
    public void saveAssertions() throws IOException {

        try {

            int i = 0;
            File tFile;
            String tPath;
            do {
                tPath = mSaveCategoryDirPath + "/" + String.valueOf(i);
                tFile = new File(tPath);
                i++;
            } while (tFile.exists());
            tFile.mkdir();

            for (int j = 0; j < mAssertions.size(); j++) {
                String tName = tFile.getPath() + "/" + String.valueOf(j) + ".txt";
                mWriter.setFilePath(tName);
                mWriter.writeOneString(mAssertions.get(j));
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }


    /**
     * Listの集合を与えたら、前件リストのすべての変数を満たす変数束縛情報を返却する.
     *
     * @return 変数束縛情報のList
     * <p>
     * <p>
     * 何もなければ空のArrayListを返す.
     * 動作はしているが無駄が多そう
     */
    public ArrayList<String> searchBinding(ArrayList<String> aAntecedents) {

        // いい名前募集中
        // ここ綺麗に書くアルゴリズム募集中
        ArrayList<String> tCandidate = new ArrayList<>();

        //candidateの初期化
        String tAn = aAntecedents.get(0);
        for (String tAs : mAssertions) {
            if (!mMatcher.canBind(tAn, tAs)) {
                continue;
            }
            tCandidate.add(mMatcher.bind(tAn, tAs));
        }

        ArrayList<String> tBuffer = new ArrayList<>();
        for (String tAntecedent : aAntecedents) {

            for (String tAssertion : mAssertions) {
                if (!mMatcher.canBind(tAntecedent, tAssertion)) {
                    continue;
                }

                String tBind = mMatcher.bind(tAntecedent, tAssertion);
                if (!tCandidate.contains(tBind)) {
                    continue;
                }
                tBuffer.add(tBind);

            }

            for (String tC : tCandidate) {
                if (!tBuffer.contains(tC)) {
                    tCandidate.remove(tC);
                }
            }
        }
        return tCandidate;
    }

    /**
     * @param aID
     * @param aHypothesys
     * @return bindできないなら、空のListが返ってくる。
     * Bindできるなら、BindしたListが返ってくる
     *
     * Listにする意味がないので、変更した
     */
    public String searchBinding(int aID, String aHypothesys) {

        if (!mMatcher.canBind(aHypothesys, mAssertions.get(aID))) {
            return "";
        }

        return mMatcher.bind(aHypothesys, mAssertions.get(aID));
    }

}