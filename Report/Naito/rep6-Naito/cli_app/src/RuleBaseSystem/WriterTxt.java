package RuleBaseSystem;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by seijihagawa on 2016/11/28.
 */
public class WriterTxt {

    private String mFilePath;

    WriterTxt() {
    }

    public void setFilePath(String aFilePath){
        mFilePath = aFilePath;
    }


    /**
     *
     * @param aSentence
     * @throws IOException
     * 確認してないけど、これ新しくファイル生成されるんだよね？
     */
    public void writeOneString(String aSentence) throws IOException{

        try {
            FileWriter filewriter = new FileWriter(new File(mFilePath));
            filewriter.write(aSentence);
            filewriter.close();
        }catch (IOException e){
            e.printStackTrace();
            throw e;
        }
    }
}

