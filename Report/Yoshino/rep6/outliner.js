/**
 * アウトラインを動的に生成する
 * Author: Shintani and Ozono Lab.
 */

;(function(window, document) {//引数は window, document
  var kLabelPrefix = 't-section-';
  var kVerticalOffset = 110;

  var TOutliner = function(headline, nav) {//TOutLiner が関数となる
    this.headlineLabels = [];
    this.navElement = document.querySelector(nav);
    this.navElement.classList.add('t-outliner-nav');

    document.body.addEventListener('click', function(event) {//文書 の body 要素にイベントリスナーを追加する クリックされた場合　ここで定義している関数を呼び出す
      if (!event.target.classList.contains('t-outliner-link')) {//引数の event が 't-outliner-link' を含んでいなければ終了
        return;
      }// if (!event.target.classList.contains('t-outliner-link'))

      event.preventDefault();//イベントがキャンセル可能である場合、上位ノードへのイベントの 伝播 (propagation) を止めずに、そのイベントをキャンセルします。
      // アウトラインのリンクがクリックされたら，ラベル部分までスクロール
      // オフセットも考慮する．kVerticalOffset で調整．
      var labelName = event.target.href.split('#').pop();//イベントを発生させた target.href でパスを入手 それで split('#')で # で区切って配列にする pop()で最後の要素(ラベル名)だけ抜き出す
      var label = this._findLabelByName(labelName);//下で定義している _findLabelByName() メソッドを呼び出す
      if (label !== null) {//null でない場合
        var bounds = label.getBoundingClientRect();
        window.scrollTo(0, bounds.top + document.body.scrollTop - kVerticalOffset);//スクロール移動する
      }//if(label !== null)
    }.bind(this)//function(event){ このオブジェクトが実行されるように bind で束縛する
	);//document.body.addEventListener( 

    var headlineElements = document.querySelectorAll(headline);
    for (var i = 0, ii = headlineElements.length; i < ii; i++) {
      var headEl = headlineElements[i];
      this.register(headEl);
    }//for
  };// function(headline, nav) 

  TOutliner.prototype = {//プロトタイプ（オブジェクト）を定義する
    _createHeadlineLabel: function() {// : によるラベル _createHeadLineLabel = function(){... と同じ
      var label = document.createElement('a');
      label.name = kLabelPrefix + (this.headlineLabels.length + 1);
      label.className = 't-outliner-label';
      this.headlineLabels.push(label);
      return label;
    },//_createHeadlineLabel: function() 

    _createLabelLink: function(headline, label) {
      var link = document.createElement('a');
      link.href = '#' + label.name;
      link.textContent = headline.textContent;
      link.className = 't-outliner-link';
      return link;
    },

    _findLabelByName: function(name) {
      for (var i = 0, ii = this.headlineLabels.length; i < ii; i++) {
        if (this.headlineLabels[i].name === name) {
          return this.headlineLabels[i];
        }//if
      }//for (var i = 0, ii = this.headlineLabels.length; i < ii; i++) 
      return null;
    },// _findLabelByName: function(name)

    register: function(headline) {
      var label = this._createHeadlineLabel();
      headline.parentNode.insertBefore(label, headline);

      var link = this._createLabelLink(headline, label);
      this.navElement.appendChild(link);
    }
  };// TOutliner.prototype = {

  window.TOutliner = TOutliner;

})(window, document);//即時実行関数式 この文は他から呼ばれなくても実行される
