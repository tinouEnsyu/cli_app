
//テキストエリアから,入力されたアサーションを得る
function input(){
  var assertion = document.as.input.value;
  return assertion;
}

// 現在のモードが前向きか後ろ向きかを判断する
function judge(){
  var forward = document.first.mode.value;
  if(forward == "前向き推論"){
    return 1;
  } else if(forward == "後ろ向き推論"){
    return 0;
  }
}

// 前向き後ろ向きのモードを切り替える
function Mode(){
  var str = judge();
  if(str == 1){
    alert("後ろ向き推論に変更します");//確認の表示
    document.first.mode.value="後ろ向き推論";//ボタンの文字を書き換える
    $("#input").attr('placeholder','ここに仮説を入力してください.');
  } else if(str == 0){
    alert("前向き推論に変更します");//確認の表示
    document.first.mode.value="前向き推論";//ボタンの文字を書き換える
    $("#input").attr('placeholder','ここにアサーションを入力してください.');
  }
}

//読み込まれたアサーションに対して,推論を実行し描画する -->
$(function Execute(){
  $('#execute').on('click', function(){
    var assertion = input();
    assertion = document.as.input.value;
    if(assertion == ""){
      alert("アサーションが入力されていません.");
      return;
    }
    alert(assertion);//質問文として送られる文をテスト表示-->
    var str = judge();
    if(str == 1){
      ajax_Forward(); //ここでjsonファイルの中身が書き換わる予定-->
    } else if(str == 0){
      ajax_Backward();//後ろ向き用-->
    }
    createNetwork();//再描画-->
  });

//ajaxを用いた通信で呼び出す予定-->
  function ajax_Forward(){
    $.ajax({

    })
  }

  function ajax_Backward(){
    $.ajax({

    })
  }
});
