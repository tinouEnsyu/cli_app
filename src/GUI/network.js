function createNetwork(){

  var nodes;
  var edges;

//read local file
  $.ajax({
       url : "./DB/workingMemory/nodes.json",
        //url : "./DB/ruleBase/ruleBase.json",
        //url : "./DB/workingMemory/workingMemory.json",
        dataType: 'json',
        cache:false,
        async:false
      }).done(function(data){
        nodes = data;
      });

//なぜか同一ディクレトリへのajaxでのアクセスを拒否される
  $.ajax({
        //url : "./DB/Edge/arc.json",
         url : "./DB/Edge/edges.json",
        dataType: 'json',
        cache:false,
        async:false
      }).done(function(data){
        edges = data;
      });

  // create a network
  var container = document.getElementById('mynetwork');
  var data = {
    nodes: nodes,
    edges: edges
  };

  var options = {};
  var network = new vis.Network(container, data, options);
  console.log(nodes);
  console.log(edges);
  //return network;
}
