package RuleBaseSystem;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.fasterxml.jackson.databind.ObjectMapper;

import config.Config;

/**
 * Created by seijihagawa on 2016/11/25.
 */
public class BackwardChain {
	private WorkingMemory mWorkingMemory;
	private RuleBase mRuleBase;
	private static String kConfigPath = "src/fonfig/config.json";
	private String mBinding;
	private Integer mAllAssertionNum;
	private Integer mAllRuleNum;
	private WriteBackwardResult mResult;

	BackwardChain(){
		try {
			Config tConfig = this.read(kConfigPath);

			String tLoadDirWorkingMemory = tConfig.getWorkingMemory().getLoadDirPath();
			String tSaveCategory = tConfig.getWorkingMemory().getSaveCategoryDir();
			mWorkingMemory = new WorkingMemory(tLoadDirWorkingMemory, tSaveCategory);

			String tLoadDirRuleBase = tConfig.getRuleBase().getLoadDirPath();
			mRuleBase = new RuleBase(tLoadDirRuleBase);
			mRuleBase.loadRules(); //これも本来はRulebaseのコンストラクタ内で処理したほうが良いかも

			mResult = new WriteBackwardResult(tSaveCategory);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private Config read(String aFilePath) throws IOException {
		try {
			FileInputStream tFile = new FileInputStream(aFilePath);
			ObjectMapper objectMapper = new ObjectMapper();
			Config tConfig = objectMapper.readValue(tFile, Config.class);
			return tConfig;
		} catch (IOException e) {
			throw e;
		}
	}


	/**
	 * コンストラクタ　初期化してから後ろ向き推論を実行（動作確認用）.
	 * @param aWorkingMemory
	 * @param aRuleBase
	 * @param aHypothesis
	 */
	BackwardChain(WorkingMemory aWorkingMemory,RuleBase aRuleBase,String aHypothesis,String tSaveCategory){
		mResult = new WriteBackwardResult(tSaveCategory);
		mWorkingMemory = aWorkingMemory;
		mAllAssertionNum = mWorkingMemory.getAllAssertionNum();
		mRuleBase = aRuleBase;
		mAllRuleNum = mRuleBase.getAllRuleNum();
		mBinding = "";
		Node firstNode = new Node(aHypothesis,-1,-1,"",null,null);


		//後ろ向き推論実行
		goBackWardChain(firstNode);
	}


	/**
	 * 後ろ向き推論実行 結果出力.
	 * @param aNode
	 */
	public void goBackWardChain(Node aNode){
		if(BackWardChain(aNode)){
			//推論成功
			System.out.println("成功　束縛:"+mBinding);
			//結果出力
			mResult.setWorkingMemory(mWorkingMemory);
			mResult.setRuleBase(mRuleBase);
			mResult.write(aNode);

		}
		else{
			//推論失敗
			System.out.println("失敗");
		}
	}

	/**
	 * 後ろ向き推論実行.
	 * @param aNode
	 * @return
	 */
	public boolean BackWardChain(Node aNode){
		Node tNode = aNode;
		Integer myNumber;

		while(true){
			if(tNode.getAssertionIndex() < mAllAssertionNum){
				//アサーションとマッチング
				if(matchNodeAssertions(tNode)){
					//マッチング成功
					//次にマッチングするノードへ移動
					while(true){
						//親ノードがなければ推論成功
						if(tNode.getParent() == null) return true;
						myNumber = getMyNumber(tNode);
						//親ノードへ移動
						tNode = tNode.getParent();
						//次にマッチングするノードの親ならbreak
						if(myNumber < tNode.getChildList().size() - 1) break;
					}
					//次の子ノードへ移動
					tNode = tNode.getChildList().get(myNumber+1);
				}
				else{
					//アサーションとのマッチングに失敗
					//アサーションとマッチングできなかったことをノードに記録する
					tNode.setAssertionIndex(mWorkingMemory.getAllAssertionNum()+2);
				}
			}
			else{
				//ルールとマッチング
				if(matchNodeRules(tNode)){
					//マッチング成功
					tNode = tNode.getChildList().get(0);
				}
				else{
					//一致するルールの後件がなかった
					tNode.setAssertionIndex(-1);
					tNode.setRuleIndex(-1);
					tNode.setBinding("");
					//親ノードがなければ推論失敗
					if(tNode.getParent() == null) return false;
					//親ノードへ移動
					tNode = tNode.getParent();

					//変数束縛を行ったノードの祖先か
					if(ancestorOfBindingNode(tNode)){
						//変数束縛を行ったノード以外の葉ノードを初期化
						while(!tNode.getChildList().isEmpty()){
							ArrayList<Node> tChildList = tNode.getChildList();
							for(Integer childNum = tNode.getChildList().size() - 1;childNum > 0;childNum--){
								Node initNode = tChildList.get(childNum);
								initNode.setAssertionIndex(-1);
								initNode.setRuleIndex(-1);
								initNode.removeChild();
							}
							tNode = tChildList.get(0);
						}
					}
					else{
						//子ノードを全て削除
						tNode.removeChild();
					}
				}
			}
		}
	}

	/**
	 * Bindできるアサーションを探す
	 * @param aNode
	 * @return
	 */
	private boolean matchNodeAssertions(Node aNode){
		if(mBinding.equals("")){
			//まだ変数束縛していない
			return matchHypothesisAssertions(0,aNode);
		}
		else{
			if(mBinding.equals(aNode.getBinding())){
				//変数束縛やりなおし
				mBinding = "";
				aNode.setBinding("");
				return matchHypothesisAssertions(aNode.getAssertionIndex()+1,aNode);
			}
			else{
				//変数束縛結果が更新されていた
				return matchHypothesisAssertions(0,aNode);
			}
		}
	}

	/**
	 * Bind出来るアサーションを探す
	 * @param aIndex 最初にマッチングするアサーションのID
	 * @param aNode
	 * @return
	 */
	private boolean matchHypothesisAssertions(Integer aIndex,Node aNode){
		String tHypothesis = aNode.getHypothesis();
		String tBinding;

		for(Integer tIndex = aIndex;tIndex < mAllAssertionNum;tIndex++){
			tBinding = mWorkingMemory.searchBinding(tIndex,tHypothesis);
			if(!tBinding.isEmpty()){
				//マッチング成功
				aNode.setAssertionIndex(tIndex);
				if(mBinding.equals("")){
					aNode.setBinding(tBinding);
					mBinding = tBinding;
				}
				return true;
			}
		}
		return false;
	}

	/**
	 * 仮説と一致するルールの後件を探す
	 * @param aNode
	 * @return
	 */
	private boolean matchNodeRules(Node aNode){
		String tHypothesis = aNode.getHypothesis();
		Integer tIndex = aNode.getRuleIndex();
		for(tIndex++;tIndex < mAllRuleNum;tIndex++){
			if(mRuleBase.canBind(tIndex,tHypothesis)){
				//後件と一致
				ArrayList<String> tAntecedents = mRuleBase.getAntecedent(tIndex);
				aNode.setRuleIndex(tIndex);
				aNode.setBinding("");
				makeChildren(aNode,tAntecedents);
				return true;
			}
		}
		return false;
	}

	/**
	 * 子ノードを生成する
	 * @param aNode 親ノード
	 * @param aAntecedents 子ノードの仮説のリスト
	 */
	private void makeChildren(Node aNode,ArrayList<String> aAntecedents){
		ArrayList<Node> tChildList = new ArrayList<Node>();
		for(Iterator<String> ti = aAntecedents.iterator();ti.hasNext();){
			tChildList.add(new Node(ti.next(),-1,-1,"",aNode,new ArrayList<Node>()));
		}
		aNode.setChildList(tChildList);
	}

	/**
	 * 変数束縛を行ったノードの祖先ならtrue
	 * @param aNode
	 * @return
	 */
	private boolean ancestorOfBindingNode(Node aNode){
		Node tNode = aNode;
		//葉ノードに付くまで0番目の子ノードを辿る
		while(!tNode.getChildList().isEmpty()){
			tNode = tNode.getChildList().get(0);
		}
		if(tNode.getBinding().equals("")) return false;
		else return true;
	}

	/**
	 * 引数のノードが親の何番目の子か返す
	 * @param aNode
	 * @return
	 */
	private Integer getMyNumber(Node aNode){
		String tHypothesis = aNode.getHypothesis();
		ArrayList<Node> tbrotherList = aNode.getParent().getChildList();

		for(Integer tChildNum = 0;;tChildNum++){
			if(tbrotherList.get(tChildNum).getHypothesis().equals(tHypothesis)){
				return tChildNum;
			}
		}
	}
}