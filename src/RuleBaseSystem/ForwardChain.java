package RuleBaseSystem;

import com.fasterxml.jackson.databind.ObjectMapper;
import config.Config;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by seijihagawa on 2016/11/24.
 */
public class ForwardChain {

    private WorkingMemory mWorkingMemory;
    private RuleBase mRuleBase;
    private static String kConfigPath = "/Users/seijihagawa/java/3rdClass/kadai6/cli_app/src/config/config.json";

    ForwardChain() {

        try {
            Config tConfig = this.read(kConfigPath);

            String tLoadDirWorkingMemory = tConfig.getWorkingMemory().getLoadDirPath();
            String tSaveCategory = tConfig.getWorkingMemory().getSaveCategoryDir();
            mWorkingMemory = new WorkingMemory(tLoadDirWorkingMemory, tSaveCategory);

            String tLoadDirRuleBase = tConfig.getRuleBase().getLoadDirPath();
            mRuleBase = new RuleBase(tLoadDirRuleBase);
            mRuleBase.loadRules(); //これも本来はRulebaseのコンストラクタ内で処理したほうが良いかも

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private Config read(String aFilePath) throws IOException {
        try {
            FileInputStream tFile = new FileInputStream(aFilePath);
            ObjectMapper objectMapper = new ObjectMapper();
            Config tConfig = objectMapper.readValue(tFile, Config.class);
            return tConfig;
        } catch (IOException e) {
            throw e;
        }
    }


    /**
     * 前向き推論のロジック
     *
     * @return tNewAssertionCreated 新しいアサーションが生成されたなら true そうでないならfalse
     * <p>
     * 命名規則でfor文回す時は、短縮系にしてみたけどどうだろう.
     * もっと効率のいいアルゴリズムあるはず.高速化したい. assertionの書き出しとかやってない気がする.
     */
    public void ForwardChain() {
        boolean tIsNewAssertion = true;

        while (tIsNewAssertion) {
            ArrayList<String> tAntecedent;
            String tConsequent;
            ArrayList<String> tBindings;

            tIsNewAssertion = false;
            for (int i_Rule = 0; i_Rule < mRuleBase.getRuleSize(); i_Rule++) {
                tAntecedent = mRuleBase.getAntecedent(i_Rule);
                tConsequent = mRuleBase.getConsequent(i_Rule);
                tBindings = mWorkingMemory.searchBinding(tAntecedent);

                for (int j_Bind = 0; j_Bind < tBindings.size(); j_Bind++) {
                    String tNewAssertion = BindSentence(tConsequent, tBindings.get(j_Bind));

                    if (mWorkingMemory.contains(tNewAssertion)) {
                        continue;
                    }

                    System.out.println("Success: " + tNewAssertion);
                    mWorkingMemory.addAssertion(tNewAssertion);
                    System.out.println("ADD:" + tNewAssertion);
                    tIsNewAssertion = true;
                }

            }
        }
        try {
            mWorkingMemory.saveAssertions();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private String BindSentence(String aVarSentence, String aBinding) {

        StringTokenizer tStringTokenizer = new StringTokenizer(aVarSentence);
        String tSentenceBuf = new String();

        for (int i = 0; i < tStringTokenizer.countTokens(); ) {
            String tToken = tStringTokenizer.nextToken();

            if (tToken.startsWith("?")) {
                tSentenceBuf = tSentenceBuf + " " + aBinding;
            } else {
                tSentenceBuf = tSentenceBuf + " " + tToken;
            }
        }

        return tSentenceBuf;
    }


}