package RuleBaseSystem;

import java.util.ArrayList;

/**
 * Created by seijihagawa on 2016/12/02.
 */
public class Node {

    private String mHypothesis;
    private int mAssertionIndex;
    private int mRuleIndex;
    private String mBinding;
    private Node mParent;
    private ArrayList<Node> mChildList;


    Node(String aHypothesis, int aAssertionIndex, int aRuleIndex,
         String aBinding, Node aParent, ArrayList<Node> aChildList){

        setHypothesis(aHypothesis);
        setAssertionIndex(aAssertionIndex);
        setRuleIndex(aRuleIndex);
        setBinding(aBinding);
        setParent(aParent);
        setChildList(aChildList);
    }

    public String getHypothesis(){
        return mHypothesis;
    }

    public int getAssertionIndex(){
        return mAssertionIndex;
    }

    public int getRuleIndex(){
        return mRuleIndex;
    }

    public String getBinding(){
        return mBinding;
    }

    public Node getParent(){
        return mParent;
    }

    public ArrayList<Node> getChildList() {
        return mChildList;
    }

    public void setHypothesis(String aHypothesis){
         mHypothesis = aHypothesis;
    }

    public void setAssertionIndex(int aAssertionIndex){
         mAssertionIndex = aAssertionIndex;
    }

    public void setRuleIndex(int aRuleIndex){
         mRuleIndex = aRuleIndex;
    }

    public void setBinding(String aBinding){
         mBinding = aBinding;
    }

    public void  setParent(Node aParent){
         mParent = aParent;
    }

    public void  setChildList(ArrayList<Node> aChildList) {
         mChildList = aChildList;
    }

    /**
     * これでListはgetできるけど、内容は空。nullを代入した方が良いのか？
     */
    public void removeChild(){
        mChildList = new ArrayList<>();
    }

}
