package RuleBaseSystem;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by seijihagawa on 2016/11/27.
 */
public class ReaderTxt {


    private String mFilePath;

    ReaderTxt() {
    }


    /**
     * 冒頭の文章のみを読み込む.
     * @return
     * @throws IOException
     *
     * Assertionのテキストの格納の仕様を意識しているのは微妙かな
     */
    public String readOneString() throws IOException {
        try {

            BufferedReader tBufReader = new BufferedReader(new FileReader(mFilePath));
            String tStr = tBufReader.readLine();
            tBufReader.close();
            return tStr;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            throw e;
        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }
    }

    public void setFilePath(String aFilePath){
        mFilePath = aFilePath;
    }

}
