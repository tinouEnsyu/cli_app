package RuleBaseSystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by seijihagawa on 2016/11/28.
 */


/**
 * @see "http://tutorials.jenkov.com/java-json/jackson-objectmapper.html#reading-json-from-a-reader"
 * jackson libraryの使い方が記載されている.
 * @see "http://hakuhin.jp/js/json.html"
 * json fileとは、が説明されている.
 * @see "http://qiita.com/opengl-8080/items/b613b9b3bc5d796c840c"
 * jacksonの書き方が記載されている
 * <p>
 * * @see "http://qiita.com/spamoc/items/69a4b8fcd0e004b3c4e8"
 * 空のコンストラクタを用意した理由
 */
public class RuleBase {

    private ArrayList<Rule> mRules;
    private String mLoadDirPath;
    private Matcher mMatcher;


    RuleBase(String aLoadDirPath) {
        mRules = new ArrayList<Rule>(50);
        this.mLoadDirPath = aLoadDirPath;
        mMatcher = new Matcher();
    }

	/**
     * @param aLoadDirPath setterは用意する必要ないかもしれない
     */
    public void setLoadDirPath(String aLoadDirPath) {
        this.mLoadDirPath = aLoadDirPath;
    }

    public void loadRules() throws IOException {
        try {

            mRules = new ArrayList<Rule>();
            File tDir = new File(mLoadDirPath);
            File[] tFiles = tDir.listFiles();

            for (int i = 0; i < tFiles.length; i++) {
                String path = mLoadDirPath + "/" + String.valueOf(i) + ".json";
                Rule tR = this.read(path);
                mRules.add(tR);
            }

        } catch (IOException e) {
            e.printStackTrace();
            throw e;
        }

    }


    private Rule read(String aFilePath) throws IOException {
        try {
            FileInputStream tFile = new FileInputStream(aFilePath);
            ObjectMapper objectMapper = new ObjectMapper();
            Rule tR = objectMapper.readValue(tFile, Rule.class);
            return tR;
        } catch (IOException e) {
            throw e;
        }
    }

    /**
     * 前件をlist化して返却する.
     *
     * @return
     */
    public ArrayList<ArrayList<String>> getAntecedents() {

        ArrayList<ArrayList<String>> tAntecedents = new ArrayList<ArrayList<String>>();
        for (Rule r : mRules) {
            tAntecedents.add(r.getAntecedents());
        }
        return tAntecedents;
    }

    /**
     * 指定されたIDのルールの後件を取得する.
     *
     * @param aRuleID これindexを意識してるのは微妙な気がする.
     *                javaってgetするとbugる？参照渡しに関わるミスなのかな？
     */
    public String getConsequent(int aRuleID) {
        return mRules.get(aRuleID).getConsequent();
    }

    /**
     * 指定したIDのルールの前件を取得する
     *
     * @param aID
     * @return
     */
    public ArrayList<String> getAntecedent(int aID) {
        return mRules.get(aID).getAntecedents();
    }


    /**
     * aIDで指定したルールの後件と一致するかどうかを返す関数
     *
     * @param aID
     * @param aHypothesys
     * @return
     */
    public boolean canBind(int aID, String aHypothesys) {
        //return mMatcher.canBind(aHypothesys, mRules.get(aID).getConsequent());
        return mMatcher.varCanBind(aHypothesys, mRules.get(aID).getConsequent());
    }

    public int getRuleSize() {
        return mRules.size();
    }

    /**
     * ルールの総数を返す
     * @author Takada
     * @return
     */
    public Integer getAllRuleNum(){
    	return mRules.size();
    }

}