package RuleBaseSystem;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class WriteBackwardResult {
	private RuleBase mRuleBase;
	private WorkingMemory mWorkingMemory;
	private ReaderTxt mReader;
	private WriterTxt mWriter;
	private String mSaveCategoryDirPath;
	
	
	WriteBackwardResult(String aSaveCategorDir){
		mReader = new ReaderTxt();
        mWriter = new WriterTxt();
        mSaveCategoryDirPath = aSaveCategorDir;
	}

	public void setRuleBase(RuleBase aRuleBase){
		mRuleBase = aRuleBase;
	}
	public void setWorkingMemory(WorkingMemory aWorkingMemory){
		mWorkingMemory = aWorkingMemory;
	}

	public void write(Node aNode){
		Node tNode = aNode;
		ArrayList tResult = new ArrayList<Rule>();
		String tgBind;
		
		StringTokenizer tTokenA;
		StringTokenizer tTokenR;
		
		Integer tChildNum;
		ArrayList<String> tAssertion;
		String tRule;
		String tVector;
		ArrayList tBind;
		//変数束縛を行ったノードへ移動
		while(!tNode.getChildList().isEmpty()){
			tNode = tNode.getChildList().get(0);
		}
		while(true){
			//仮説からアサーション
			tAssertion = new ArrayList<String>();
			tBind = new ArrayList<String>();
			if(tNode.getChildList().isEmpty()){
				tAssertion.add(tNode.getHypothesis());
				tRule = mWorkingMemory.getAsertion(tNode.getAssertionIndex());
				tVector ="to";
				tTokenA = new StringTokenizer(tAssertion.get(0)," ");
				tTokenR = new StringTokenizer(tRule," ");
				tBind.add(tTokenA.nextToken() + "=" + tTokenR.nextToken());
				tResult.add(new Result(tAssertion,tRule,tVector,tBind));
			}
			
			//親ノードがなければ終了
			if(tNode.getParent() == null){
				writeResult(tResult);
				return;
			}
			
			//ルールから仮説
			tAssertion = new ArrayList<String>();
			tBind = new ArrayList<String>();
			tAssertion.add(tNode.getHypothesis());
			tRule = ifString(tNode.getParent().getRuleIndex());
			tVector = "from";
			tBind.add("");
			tResult.add(new Result(tAssertion,tRule,tVector,tBind));
			
			
			//親ノードの何番目の子か
			tChildNum = getMyNumber(tNode);
			//親ノードへ移動
			tNode = tNode.getParent();
			
			
			//まだ結果を取り出していない子ノードがある
			if(!(tChildNum == tNode.getChildList().size() - 1)){
				tNode = tNode.getChildList().get(tChildNum+1);
				while(!tNode.getChildList().isEmpty()){
					tNode = tNode.getChildList().get(0);
				}
			}
			else{
				//仮説からルール
				tAssertion = new ArrayList<String>();
				tBind = new ArrayList<String>();
				tAssertion.add(tNode.getChildList().get(tChildNum).getHypothesis());
				tRule = ifString(tNode.getRuleIndex());
				tVector = "to";
				tBind.add("");
				tResult.add(new Result(tAssertion,tRule,tVector,tBind));
				
			}
		}
	}
	
	/**
	 * 結果をファイルに書き出す
	 * @param aResult 
	 */
	private void writeResult(ArrayList<Result> aResult){

        try {

            int i = 0;
            File tFile;
            String tPath;
            do {
                tPath = mSaveCategoryDirPath + "/" + String.valueOf(i);
                tFile = new File(tPath);
                i++;
            } while (tFile.exists());
            tFile.mkdir();

            for (int j = 0; j < aResult.size(); j++) {
                String tName = tFile.getPath() + "/" + String.valueOf(j) + ".txt";
                mWriter.setFilePath(tName);
                mWriter.writeOneString("{\n\"assertion\":[\""+aResult.get(j).getAssertions().get(0)+"\"],\n" +
                		"\"Rule\":\""+aResult.get(j).getRule()+"\",\n" +
                				"\"Vector\":\""+aResult.get(j).getVector()+"\",\n"+
                				"\"bind\":[\""+aResult.get(j).getBinds()+"\"]");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
	}
	
	/**
	 * 指定したIDのルールの前件をjsonで使うStringにして返す
	 * @param aID
	 * @return
	 */
	private String ifString(Integer aID){
		ArrayList<String> tAntecedent = mRuleBase.getAntecedent(aID);
		
		String tString = "if   \""+tAntecedent.get(0)+"\"";
		for(Integer ti = 1;ti < tAntecedent.size();ti++){
			tString = tString+"\n     \""+tAntecedent.get(ti)+"\"";
		}
		tString = tString+"\nthen \""+mRuleBase.getConsequent(aID)+"\"";
		return tString;
	}

/**
 * 引数のノードが親の何番目の子か返す
 * @param aNode
 * @return
 */
private Integer getMyNumber(Node aNode){
	String tHypothesis = aNode.getHypothesis();
	ArrayList<Node> tbrotherList = aNode.getParent().getChildList();
	
	for(Integer tChildNum = 0;;tChildNum++){
		if(tbrotherList.get(tChildNum).getHypothesis().equals(tHypothesis)){
			return tChildNum;
		}
	}
}
}