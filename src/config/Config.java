package config;

/**
 * Created by seijihagawa on 2016/11/24.
 */

import RuleBaseSystem.RuleBase;
import com.fasterxml.jackson.annotation.JsonGetter;
import com.fasterxml.jackson.annotation.JsonSetter;

import java.util.ArrayList;


/**
 *
 */


public class Config {
    private workingMemory mWorkingMemory;
    private ruleBase mRuleBase;

    Config() {

    }

    public static class workingMemory {

        private String mLoadDirPath;
        private String mSaveCategoryDir;

        @JsonSetter("LoadDirPath")
        public void setLoadDirPath(String aDirPath) {
            mLoadDirPath = aDirPath;
        }

        @JsonSetter("SaveCategoryDir")
        public void setSaveCategoryDir(String aDir) {
            mSaveCategoryDir = aDir;
        }

        //このアノテーションいらんかも
        @JsonGetter("LoadDirPath")
        public String getLoadDirPath() {
            return mLoadDirPath;
        }

        public String getSaveCategoryDir() {
            return mSaveCategoryDir;
        }

    }

    @JsonSetter("WorkingMemory")
    public void setWorkingMemory(workingMemory aWm) {
        mWorkingMemory = aWm;
    }

    public workingMemory getWorkingMemory() {
        return mWorkingMemory;
    }

    public static class ruleBase {
        private String mLoadDirPath;

        @JsonSetter("LoadDirPath")
        public void setLoadDirPath(String aDirPath) {
            mLoadDirPath = aDirPath;
        }

        public String getLoadDirPath() {
            return mLoadDirPath;
        }
    }


    @JsonSetter("RuleBase")
    public void setRuleBase(ruleBase aRule) {
        mRuleBase = aRule;
    }

    public ruleBase getRuleBase() {
        return mRuleBase;
    }


}